# Mirror
This is a GitHub mirror of the [original repository](https://codeberg.org/Expo/sbjs) on Codeberg. Do not submit pull requests or issues here.

This mirror can be stale. Do not rely on it. It is only updated on deploy.

**Seriously: You will not get PRs merged here. Move to codeberg for this.** I'm just using this as codeberg pages doesn't consistently work. Issues also just belong there.
